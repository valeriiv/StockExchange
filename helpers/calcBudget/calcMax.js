import { Decimal } from 'decimal.js';

/**
 * calculate max transaction value
 * @param arr
 * @returns {string}
 */
export const calcMax = arr => {
  if (arr.length) {
    const allSumm = arr.map(item => item.summ);
    const result = Math.max(...allSumm);
    return new Decimal(result).toFixed(2);
  }
  return new Decimal(0.0).toFixed(2);
};
