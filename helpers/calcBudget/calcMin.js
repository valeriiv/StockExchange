import { Decimal } from 'decimal.js';

/**
 * calculate min transaction value
 * @param arr
 * @returns {string}
 */
export const calcMin = arr => {
  if (arr.length) {
    const allSumm = arr.map(item => item.summ);
    const result = Math.min(...allSumm);
    return new Decimal(result).toFixed(2);
  }
  return new Decimal(0.0).toFixed(2);
};
