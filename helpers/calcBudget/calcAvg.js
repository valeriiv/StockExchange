import { Decimal } from 'decimal.js';

/**
 * calculate average
 * @param arr
 * @returns {string}
 */
export const calcAvg = arr => {
  if (arr.length) {
    const allSumm = arr
      .map(item => Number(item.summ))
      .reduce((accumulator, curVal) => accumulator + curVal, 0);
    // eslint-disable-next-line
    console.log("calcAvg, allSumm = ", allSumm);
    const result = allSumm / arr.length;
    // eslint-disable-next-line
    console.log("calcAvg, result = ", result);
    return new Decimal(result).toFixed(2);
  }
  return new Decimal(0.0).toFixed(2);
};
