import { Decimal } from 'decimal.js';

/**
 * calculate remaining balance
 * @param limit
 * @param transactionSumm
 * @returns {number}
 */
export const calcRemainingBalance = (limit, transactionSumm) => {
  const result =
    new Decimal(limit).toFixed(2) - new Decimal(transactionSumm).toFixed(2);
  if (result > 0) {
    return result;
  }
  return -1;
};
