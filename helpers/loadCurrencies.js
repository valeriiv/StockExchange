import axios from 'axios';

export async function get3Currencies(reqString, corsString = '') {
  let request;
  try {
    if (process.env.NODE_ENV === 'development') {
      request = await axios.get(`${corsString}${reqString}`, {
        mode: 'cors'
      });
    } else {
      request = await axios.get(`${reqString}`, {
        mode: 'cors'
      });
    }
    return request;
  } catch (err) {
    // eslint-disable-next-line
      console.log(err);
  }
}

export async function getHryvnya(reqString, corsString = '') {
  let request;
  try {
    if (process.env.NODE_ENV === 'development') {
      request = await axios.get(`${corsString}${reqString}`, {
        mode: 'cors'
      });
    } else {
      request = await axios.get(`${reqString}`, {
        mode: 'cors'
      });
    }
    return request;
  } catch (err) {
    // eslint-disable-next-line
      console.log(err);
  }
}
