/**
 * format current Date by pattern 'YYYY-M-D'
 * incoming type: Date
 * returned type: String
 *
 * @returns 'YYYY-M-D'
 */
export const formatDate = () => {
  const currentDate = new Date();
  let resultMonth = '';
  const currentMonth = currentDate.getMonth() + 1;
  if (currentMonth < 10) {
    resultMonth = '0' + currentMonth;
  } else {
    resultMonth = currentMonth;
  }
  const resultDay = currentDate.getDate();
  const resultYear = currentDate.getFullYear();
  const formatedDate = `${resultYear}-${resultMonth}-${resultDay}`;
  return formatedDate;
};
