export const receiveFromLS = budgets => {
  const BUDGETS_KEY = 'budgets';
  if (process.browser) {
    if (localStorage.getItem(BUDGETS_KEY)) {
      Object.assign(budgets, JSON.parse(localStorage.getItem(BUDGETS_KEY)));
    }
  }
};
