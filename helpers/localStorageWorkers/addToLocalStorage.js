export const addtoLS = budget => {
  const BUDGETS_KEY = 'budgets';
  try {
    localStorage.setItem(BUDGETS_KEY, JSON.stringify(budget));
  } catch (e) {
    // eslint-disable-next-line
    console.log(e);
  }
};
