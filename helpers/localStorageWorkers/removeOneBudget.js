export const removeBudgetLS = (budgets, delKey) => {
  const BUDGETS_KEY = 'budgets';
  budgets.splice(delKey, 1);
  try {
    localStorage.setItem(BUDGETS_KEY, JSON.stringify(budgets));
  } catch (e) {
    // eslint-disable-next-line
        console.error(e);
  }
};
