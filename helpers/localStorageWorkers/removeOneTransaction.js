export const removeTransactionLS = (budgets, payload) => {
  const BUDGETS_KEY = 'budgets';
  try {
    localStorage.setItem(BUDGETS_KEY, JSON.stringify(budgets));
  } catch (e) {
    // eslint-disable-next-line
        console.error(e);
  }
};
