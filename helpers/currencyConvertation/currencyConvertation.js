import { Decimal } from 'decimal.js';

export const converter = (budgetCurrency, transCurrency, rate, transSumm) => {
  // eslint-disable-next-line
  console.log('rate = ', rate);
  let convertedSumm;
  if (budgetCurrency === 'RUB') {
    switch (transCurrency) {
      case 'USD':
        return (convertedSumm =
          (new Decimal(transSumm) * new Decimal(rate.rub)) /
          new Decimal(rate.usd));
    }
  }
  return convertedSumm;
};
