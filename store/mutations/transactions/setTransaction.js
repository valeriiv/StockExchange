/* eslint-disable */
import { Decimal } from 'decimal.js';
import { addtoLS } from '../../../helpers/localStorageWorkers/addToLocalStorage';
// import { addTransactiontoLS } from '../../helpers/localStorageWorkers/addTransactionsToLS';

export const SET_TRANSACTION = (state, payload) => {
  let { currentTransactions } = state;
  // eslint-disable-next-line
  const { currency, name, summ  } = payload;
  currentTransactions.push({ currency, name, summ  });
  const budgets = state.budgets.map((item, ind) => {
    if(ind === +currentBudgetId) {
      return { ...item, transactions: currentTransactions }
    }
    return item;
  });
  state.budgets = budgets;

  let transactions = [];
  let budgetLimit = 0;
  let budgetCurrency = '';
  budgets.forEach((item, ind) => {
    if (ind === Number(currentBudgetId)) {
      budgetLimit = item.limit;
      budgetCurrency = item.currency;
      transactions = item.transactions;
    }
  });

  let rem_budg = 0;
  let avgSumm = 0;
  let maxSumm = 0;
  let minSumm = 0;
  if (budgetCurrency === currency) {
    rem_budg = calcRemainingBalance(budgetLimit, summ);
    avgSumm = calcAverage(transactions);
    maxSumm = calcMax(transactions);
    minSumm = calcMin(transactions);
  }

  budgets.forEach((item, ind) => {
    if (ind === Number(currentBudgetId)) {
      item.rem_budg = new Decimal(rem_budg).toFixed(2);
      item.avgSumm = new Decimal(avgSumm).toFixed(2);
      item.maxSumm = new Decimal(maxSumm).toFixed(2);
      item.minSumm = new Decimal(minSumm).toFixed(2);
      item.transactions.push(payload);
      // addTransactiontoLS(item.budgetName);
    }
  });

  addtoLS(budgets);
};

/**
 * calculate remaining balance
 * @param limit
 * @param transactionSumm
 * @returns {number}
 */
function calcRemainingBalance(limit, transactionSumm) {
  const result =
    new Decimal(limit).toFixed(2) - new Decimal(transactionSumm).toFixed(2);
  if (result > 0) {
    return result;
  }
  return -1;
}

/**
 * calculate average
 * @param arr
 * @returns {string}
 */
function calcAverage(arr) {
  if (arr.length) {
  const allSumm = arr.map(item => item.summ).reduce((accumulator, curVal) => accumulator + curVal, 0);
  const result = allSumm / arr.length;
  return new Decimal(result).toFixed(2);
  }
  return new Decimal(0.0).toFixed(2);
}

/**
 * calculate max transaction value
 * @param arr
 * @returns {string}
 */
function calcMax(arr) {
  if (arr.length) {
    const allSumm = arr.map(item => item.summ);
    const result = Math.max(...allSumm);
    return new Decimal(result).toFixed(2);
  }
  return new Decimal(0.0).toFixed(2);
}

  /**
   * calculate min transaction value
   * @param arr
   * @returns {string}
   */
function calcMin(arr) {
  if (arr.length) {
  const allSumm = arr.map(item => item.summ);
  const result = Math.min(...allSumm);
  return new Decimal(result).toFixed(2);
  }
  return new Decimal(0.0).toFixed(2);
}
