import { removeTransactionLS } from '../../../helpers/localStorageWorkers/removeOneTransaction';

export const DELETE_SELECTED_TRANSACTION = (state, payload) => {
  const { ind: delKey, currentIndex } = payload;

  state.budgets[currentIndex].transactions.splice(delKey, 1);
  state.currentTransactions.splice(delKey, 1);

  removeTransactionLS(state.budgets, payload);
};
