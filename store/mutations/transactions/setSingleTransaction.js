import { addtoLS } from '../../../helpers/localStorageWorkers/addToLocalStorage';

export const SET_SINGLE_TRANSACTION = (state, payload) => {
  const { currentTransactions } = state;
  const { currency, name, summ } = payload;
  currentTransactions.push({ currency, name, summ });
  state.currentTransactions = currentTransactions;

  const index = Number(state.currentBudgetId);
  state.budgets[index].transactions = [...currentTransactions];

  addtoLS(state.budgets);
};
