export const RECALC_TRANSACTIONS = (state, payload) => {
  const currentTransactions = state.currentTransactions;

  const usd = Number(state.USD);
  const euro = Number(state.EURO);
  const gbp = Number(state.GBP);
  const rub = Number(state.RUB);
  // eslint-disable-next-line
  console.log('rub = ', rub);
  const hrv = Number(state.HRV);

  const recalcTransactions = [];
  currentTransactions.forEach(item => {
    const transCurrency = item.currency;
    // eslint-disable-next-line
    console.log('transCurrency = ', transCurrency);
    const summ;
    switch (transCurrency) {
      case 'USD':
        summ = ((Number(item.summ) * rub) / usd).toFixed(2);
        // eslint-disable-next-line
        console.log('item.summ in map = ', item.summ);
        break;
      case 'EURO':
        item.summ = Number(item.summ) * rub * euro;
        break;
      case 'GBP':
        item.summ = (Number(item.summ) * rub) / gbp;
        break;
      case 'HRV':
        const ratio = rub / hrv;
        item.summ = Number(item.summ) * ratio;
        break;
    }
    // eslint-disable-next-line
    console.log('item = ', item);
    recalcTransactions.push(item);
  });
  // eslint-disable-next-line
  console.log('recalcTransactions = ', recalcTransactions);
  state.recalcTransactions = recalcTransactions;
};
