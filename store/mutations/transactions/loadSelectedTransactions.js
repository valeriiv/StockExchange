export const LOAD_SELECTED_TRANSACTONS = (state, payload) => {
  const index = Number(payload);
  if (state.budgets.length !== 0) {
    state.currentTransactions = [...state.budgets[index].transactions];
  } else {
    state.currentTransactions = [];
  }
};
