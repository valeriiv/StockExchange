export const SET_CURRENCIES = (state, payload) => {
  const { USD, EURO, GBP, RUB, HRV } = payload;
  state.USD = USD;
  state.EURO = EURO;
  state.GBP = GBP;
  state.RUB = RUB;
  state.HRV = HRV;
};
