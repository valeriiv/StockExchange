import { removeBudgetLS } from '../../../helpers/localStorageWorkers/removeOneBudget';

export const DELETE_SELECTED_BUDGET = ({ budgets }, payload) => {
  removeBudgetLS(budgets, payload);
};
