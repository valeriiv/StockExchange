import { receiveFromLS } from '../../../helpers/localStorageWorkers/receiveFromLocalStorage';

export const RECEIVE_BUDGETS = (state, payload) => {
  receiveFromLS(state.budgets);
};
