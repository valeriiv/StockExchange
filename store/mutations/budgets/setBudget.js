import { Decimal } from 'decimal.js';
import { addtoLS } from '../../../helpers/localStorageWorkers/addToLocalStorage';

export const SET_BUDGET = ({ budgets }, payload) => {
  const { currency, limit, name } = payload;
  const resultSingleBudget = {
    remBudget: new Decimal(0.0).toFixed(2),
    avgSumm: new Decimal(0.0).toFixed(2),
    minSumm: new Decimal(0.0).toFixed(2),
    maxSumm: new Decimal(0.0).toFixed(2),
    currency,
    limit: new Decimal(limit).toFixed(2),
    name,
    transactions: []
  };
  budgets.push(resultSingleBudget);
  addtoLS(budgets);
};
