export const UPDATE_BUDGETS = (state, payload) => {
  const index = Number(state.currentBudgetId);

  // eslint-disable-next-line
  console.log("payload = ", payload);

  state.budgets[index].minSumm = payload.minSumm;
  state.budgets[index].maxSumm = payload.maxSumm;
  state.budgets[index].avgSumm = payload.avgSumm;
  state.budgets[index].remBudget = payload.remBudget;
};
