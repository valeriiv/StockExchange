import Vue from 'vue';
import Vuex from 'vuex';
import { Decimal } from 'decimal.js';
import * as actions from './actions';
import * as getters from './getters';
import * as mutations from './mutations';

Vue.use(Vuex);

const state = () => ({
  USD: new Decimal(0.0).toFixed(2),
  EURO: new Decimal(0.0).toFixed(2),
  GBP: new Decimal(0.0).toFixed(2),
  RUB: new Decimal(0.0).toFixed(2),
  HRV: new Decimal(0.0).toFixed(2),
  budgets: [],
  currentBudgetId: 0,
  currentTransactions: [],
  recalcTransactions: []
});

export default {
  state,
  actions,
  getters,
  mutations
};
