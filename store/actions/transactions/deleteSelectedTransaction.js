export const deleteSelectedTransaction = ({ commit }, payload) => {
  commit('DELETE_SELECTED_TRANSACTION', payload);
};
