import { calcMin } from '../../../helpers/calcBudget/calcMin';
import { calcMax } from '../../../helpers/calcBudget/calcMax';
import { calcAvg } from '../../../helpers/calcBudget/calcAvg';
import { calcRemainingBalance } from '../../../helpers/calcBudget/calcRemainingBalance';
import { converter } from '../../../helpers/currencyConvertation/currencyConvertation';

export const setTransaction = ({ state, commit }, payload) => {
  commit('SET_SINGLE_TRANSACTION', payload);

  // eslint-disable-next-line
  // console.log(payload);
  const index = Number(state.currentBudgetId);
  const { currency, summ } = payload;
  // eslint-disable-next-line
  let transactions = state.currentTransactions;
  // eslint-disable-next-line
  // console.log('currency = ', currency);
  const currentBudgetCurrency = state.budgets[index].currency;
  const currentBudgetLimit = state.budgets[index].limit;
  // eslint-disable-next-line
  // console.log('currentBudgetCurrency = ', currentBudgetCurrency);
  // eslint-disable-next-line
  // console.log('transactions = ', transactions);
  let minSumm = 0;
  let maxSumm = 0;
  let avgSumm = 0;
  let remBudget = 0;
  let updatedBudget = {};
  if (currentBudgetCurrency === currency) {
    minSumm = calcMin(transactions);
    maxSumm = calcMax(transactions);
    avgSumm = calcAvg(transactions);
    remBudget = calcRemainingBalance(currentBudgetLimit, summ);
    // eslint-disable-next-line
    console.log('remBudget = ', remBudget);
  } else {
    const rate = {
      usd: state.USD,
      euro: state.EURO,
      gbp: state.GBP,
      rub: state.RUB,
      hrv: state.HRV
    };

    const convertedSumm = converter(
      currentBudgetCurrency,
      currency,
      rate,
      summ
    );

    commit('RECALC_TRANSACTIONS');

    minSumm = calcMin(state.recalcTransactions);
    maxSumm = calcMax(state.recalcTransactions);
    avgSumm = calcAvg(state.recalcTransactions);
    remBudget = calcRemainingBalance(currentBudgetLimit, convertedSumm);
  }
  updatedBudget = { minSumm, maxSumm, avgSumm, remBudget };
  commit('UPDATE_BUDGETS', updatedBudget);
};
