export const currentBudgetId = ({ commit }, payload) => {
  commit('CURRENT_BUDGET_ID', payload);
};
