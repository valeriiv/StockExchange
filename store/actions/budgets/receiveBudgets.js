export const receiveBudgets = ({ commit }, payload) => {
  commit('RECEIVE_BUDGETS', payload);
};
