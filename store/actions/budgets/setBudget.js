/**
 *
 * load data from api.exchangeratesapi.io
 * @param {*} { commit }
 * @param {*} payload
 */
export const setBudget = ({ commit }, payload) => {
  commit('SET_BUDGET', payload);
};
