export const deleteSelectedBudget = ({ commit }, payload) => {
  commit('DELETE_SELECTED_BUDGET', payload);
};
