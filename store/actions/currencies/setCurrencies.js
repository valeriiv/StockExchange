import { Decimal } from 'decimal.js';
import { formatDate } from '../../../helpers/formatDate';
import { get3Currencies, getHryvnya } from '../../../helpers/loadCurrencies';

export const setCurrencies = ({ commit }, payload) => {
  let result;
  let hrResult = '';
  const date = formatDate();
  const corsString = 'https://cors-anywhere.herokuapp.com/';
  const reqString = `https://api.exchangeratesapi.io/${date}`;
  const hrvString = 'https://banker.ua/marketindex/forex/eur-uah/';
  /**
   * get currencies every 30 seconds
   */
  setInterval(async () => {
    result = await get3Currencies(reqString, corsString);
    const { GBP, RUB, USD } = result.data.rates;
    hrResult = await getHryvnya(hrvString, corsString);
    const parsedHryvnya = hrResult.data.match(
      /id="value2"([^>]+)autocomplete/g
    )[0];
    const HRV = parsedHryvnya
      .split(' ')[5]
      .match(/([0-9.]+)/)[0]
      .replace(/"/g, '');
    const EUR = 1;
    const currencies = {
      USD: new Decimal(USD).toFixed(2),
      EURO: new Decimal(EUR).toFixed(2),
      GBP: new Decimal(GBP).toFixed(2),
      RUB: new Decimal(RUB).toFixed(2),
      HRV: new Decimal(HRV).toFixed(2)
    };
    commit('SET_CURRENCIES', currencies);
  }, 30000);
};
